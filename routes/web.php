<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

// Redirect from home to Elon Musk's Tweet Table.
Route::redirect('/', '/elonmusk');

/**
 * This routing group represents what is possible when the time comes to implement the same types of data
 * representations on other social media platforms. With a little more work in the templates to remove the
 * Twitter-specific terminology, these groups could be quickly and easily replicated for any number of other services.
 * Within this group, the `TwitterRepository` acts as the `SocialRepository`. The `TwitterRepository` does the
 * service-specific work of building `SocialPostCollection` and `SocialConnectionCollection` models from the data and
 * structures provided by the Twitter API.
 */
Route::name('twitter.')->group(function () {
    // Bind Twitter as the repository for this group of routes.
    app()->bind(\App\Repositories\SocialRepository::class, \App\Repositories\TwitterRepository::class);

    Route::get('/{handle}', 'SocialPostsController')->name('posts');
    Route::get('/{handle}/followers', 'SocialConnectionsController')->name('connections');
});

