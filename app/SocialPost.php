<?php
/**
 * @author David Cochrum <davidcochrum@gmail.com>
 */

namespace App;

/**
 * Represents a single post on social media.
 *
 * @package App
 */
class SocialPost
{
    /**
     * Contains content shared within post.
     *
     * @var string
     */
    protected $_content;

    /**
     * Contains number of times post was "liked".
     *
     * @var int
     */
    protected $_likes;

    /**
     * Contains number of times post was shared.
     *
     * @var int
     */
    protected $_shares;

    /**
     * Contains representation of various attributes of the post.
     *
     * @var Dimensions
     */
    protected $_dimensions;

    /**
     * Contains representation of various quantitative measurements of the post.
     *
     * @var Metrics
     */
    protected $_metrics;

    /**
     * Default constructor.
     *
     * @param string     $content    Content shared within post.
     * @param int        $likes      Number of times post was "liked".
     * @param int        $shares     Number of times post was shared.
     * @param Dimensions $dimensions Representation of various attributes of the post.
     * @param Metrics    $metrics    Representation of various quantitative measurements of the post.
     */
    public function __construct(string $content, int $likes, int $shares, Dimensions $dimensions, Metrics $metrics)
    {
        $this->_content = $content;
        $this->_likes = $likes;
        $this->_shares = $shares;
        $this->_dimensions = $dimensions;
        $this->_metrics = $metrics;
    }

    /**
     * Provides content shared within post.
     *
     * @return string
     */
    public function getContent() : string
    {
        return $this->_content;
    }

    /**
     * Provides number of times post was "liked".
     *
     * @return int
     */
    public function getLikes() : int
    {
        return $this->_likes;
    }

    /**
     * Provides number of times post was shared.
     *
     * @return int
     */
    public function getShares() : int
    {
        return $this->_shares;
    }

    /**
     * Provides representation of various attributes of the post.
     *
     * @return Dimensions
     */
    public function getDimensions() : Dimensions
    {
        return $this->_dimensions;
    }

    /**
     * Provides representation of various quantitative measurements of the post.
     *
     * @return Metrics
     */
    public function getMetrics() : Metrics
    {
        return $this->_metrics;
    }
}
