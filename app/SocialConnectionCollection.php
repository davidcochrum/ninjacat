<?php
/**
 * @author David Cochrum <davidcochrum@gmail.com>
 */

namespace App;

use Illuminate\Support\Collection;

/**
 * Represents a collection of social media connections to a specific user.
 *
 * @package App
 */
class SocialConnectionCollection extends Collection
{
    /**
     * Contains the user's display name.
     *
     * @var string
     */
    protected $_userName;

    /**
     * Contains the user's identifier.
     *
     * @var string
     */
    protected $_userHandle;

    /**
     * Contains the URL to view the user's profile.
     *
     * @var string
     */
    protected $_userUrl;

    /**
     * Contains total number of connections to user.
     *
     * @var int
     */
    protected $_total;

    /**
     * Contains cursor value to access previous page of results.
     *
     * @var int
     */
    protected $_prev;

    /**
     * Contains cursor value to access next page of results.
     *
     * @var int
     */
    protected $_next;

    /**
     * Default constructor.
     *
     * @param string             $userName    The user's display name.
     * @param string             $userHandle  The user's identifier.
     * @param string             $userUrl     The URL to view the user's profile.
     * @param int                $total       Total number of connections to user.
     * @param int                $prev        Cursor value to access previous page of results.
     * @param int                $next        Cursor value to access next page of results.
     * @param SocialConnection[] $connections Optional list of connection objects to initialize with.
     */
    public function __construct(
        string $userName,
        string $userHandle,
        string $userUrl,
        int $total,
        int $prev,
        int $next,
        array $connections = []
    ) {
        parent::__construct($connections);

        $this->_userName = $userName;
        $this->_userHandle = $userHandle;
        $this->_userUrl = $userUrl;
        $this->_total = $total;
        $this->_prev = $prev;
        $this->_next = $next;
    }

    /**
     * Provides the user's display name.
     *
     * @return string
     */
    public function getUserName() : string
    {
        return $this->_userName;
    }

    /**
     * Provides the user's identifier.
     *
     * @return string
     */
    public function getUserHandle() : string
    {
        return $this->_userHandle;
    }

    /**
     * Provides the URL to view the user's profile.
     *
     * @return string
     */
    public function getUserUrl() : string
    {
        return $this->_userUrl;
    }

    /**
     * Provides total number of connections to user.
     *
     * @return int
     */
    public function getTotal() : int
    {
        return $this->_total;
    }

    /**
     * Provides cursor value to access previous page of results.
     *
     * @return int
     */
    public function getPrev() : int
    {
        return $this->_prev;
    }

    /**
     * Provides cursor value to access next page of results.
     *
     * @return int
     */
    public function getNext() : int
    {
        return $this->_next;
    }
}
