<?php
/**
 * @author David Cochrum <davidcochrum@gmail.com>
 */

namespace App\Repositories;

use App\Dimensions;
use App\Metrics;
use App\SocialConnection;
use App\SocialConnectionCollection;
use App\SocialPost;
use App\SocialPostCollection;
use Carbon\Carbon;
use Thujohn\Twitter\Twitter;

/**
 * Provides social media data sourced from Twitter.
 *
 * @package App\Repositories
 */
class TwitterRepository implements SocialRepository
{
    /**
     * Contains Twitter API wrapper.
     *
     * @var Twitter
     */
    protected $_api;

    /**
     * Default constructor.
     *
     * @param Twitter $api Twitter API wrapper.
     */
    public function __construct(Twitter $api)
    {
        $this->_api = $api;
    }

    /**
     * @inheritDoc
     */
    public function getPostsFromHandle(string $handle, int $limit = 100) : SocialPostCollection
    {
        // Fetch the raw API data. Exclude replies and retweets as per requirements.
        $data = $this->_api->getUserTimeline(['screen_name' => $handle, 'count' => $limit, 'exclude_replies' => true,
            'include_rts' => false]);

        // Process raw API data into social media post collection.
        $author = head($data)->user;
        $collection = new SocialPostCollection($author->name, $author->screen_name, $this->_api->linkUser($author));
        foreach ($data as $postData) {
            $collection[] = new SocialPost(
                $postData->text,
                $postData->favorite_count,
                $postData->retweet_count,
                new Dimensions(
                    Carbon::createFromTimeString($postData->created_at),
                    $postData->lang,
                    $postData->geo,
                    $postData->coordinates
                ),
                new Metrics()
            );
        }

        return $collection;
    }

    /**
     * @inheritDoc
     */
    public function getConnectionsForHandle(
        string $handle,
        int $limit = 100,
        int $cursor = -1
    ) : SocialConnectionCollection {
        // First, fetch the raw API data about the user.
        $userData = $this->_api->getUsersLookup(['screen_name' => $handle]);
        $user = head($userData);

        // Now fetch the raw API data for their followers.
        $data = $this->_api->getFollowers(['user_id' => $user->id, 'cursor' => $cursor, 'count' => $limit,
            'skip_status' => true, 'include_user_entities' => false]);

        // Build a social connection collection from the raw data.
        $collection = new SocialConnectionCollection($user->name, $user->screen_name, $this->_api->linkUser($user),
            $user->followers_count, $data->previous_cursor, $data->next_cursor);
        foreach ($data->users as $connectionData) {
            $collection[] = new SocialConnection($connectionData->name, $connectionData->screen_name);
        }

        return $collection;
    }
}
