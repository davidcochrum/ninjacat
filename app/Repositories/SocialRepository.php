<?php
/**
 * @author David Cochrum <davidcochrum@gmail.com>
 */

namespace App\Repositories;

use App\SocialConnectionCollection;
use App\SocialPostCollection;

/**
 * Defines methods available for fetching data sourced by social media profiles.
 *
 * @todo Add more methods for fetching other types of social media data.
 *
 * @package App\Repositories
 */
interface SocialRepository
{
    /**
     * Provides a collection of social media posts made by a user.
     *
     * @param string $handle User identifier.
     * @param int    $limit  Maximum number of posts to retrieve.
     *
     * @return SocialPostCollection
     */
    public function getPostsFromHandle(string $handle, int $limit = 100) : SocialPostCollection;

    /**
     * Provides a collection of social media connections to a user.
     *
     * @param string $handle User identifier.
     * @param int    $limit  Maximum number of connections to retrieve.
     * @param int    $cursor Current position within chunked list.
     *
     * @return SocialConnectionCollection
     */
    public function getConnectionsForHandle(
        string $handle,
        int $limit = 100,
        int $cursor = -1
    ) : SocialConnectionCollection;
}
