<?php
/**
 * @author David Cochrum <davidcochrum@gmail.com>
 */

namespace App;

/**
 * Representation of various quantitative measurements of a portion of data.
 *
 * @package App
 */
class Metrics
{
    /**
     * Contains number of sessions.
     *
     * @var int
     */
    private $_sessions;

    /**
     * Contains number of views.
     *
     * @var int
     */
    private $_views;

    /**
     * Default constructor.
     *
     * @param int $sessions Number of sessions.
     * @param int $views    Number of views.
     */
    public function __construct(int $sessions = 0, int $views = 0)
    {
        $this->_sessions = $sessions;
        $this->_views = $views;
    }

    /**
     * Provides number of sessions.
     *
     * @return int
     */
    public function getSessions()
    {
        return $this->_sessions;
    }

    /**
     * Provides number of views.
     *
     * @return int
     */
    public function getViews()
    {
        return $this->_views;
    }

    /**
     * Provides average number of views per session.
     *
     * @return float
     */
    public function getAverageViewsPerSession() : float
    {
        return $this->_views / $this->_sessions;
    }
}
