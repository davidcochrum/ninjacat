<?php
/**
 * @author David Cochrum <davidcochrum@gmail.com>
 */

namespace App;

/**
 * Represents a single social media connection to a specific user.
 *
 * @package App
 */
class SocialConnection
{
    /**
     * Contains display name for connection.
     *
     * @var string
     */
    protected $_name;

    /**
     * Contains identifier for connection.
     *
     * @var int
     */
    protected $_handle;

    /**
     * Default constructor.
     *
     * @param string $name   Display name for connection.
     * @param string $handle Identifier for connection.
     */
    public function __construct(string $name, string $handle)
    {
        $this->_name = $name;
        $this->_handle = $handle;
    }

    /**
     * Provides display name for connection.
     *
     * @return string
     */
    public function getName() : string
    {
        return $this->_name;
    }

    /**
     * Provides identifier for connection.
     *
     * @return string
     */
    public function getHandle() : string
    {
        return $this->_handle;
    }
}
