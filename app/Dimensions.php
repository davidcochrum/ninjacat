<?php
/**
 * @author David Cochrum <davidcochrum@gmail.com>
 */

namespace App;

use Carbon\Carbon;

/**
 * Representation of various attributes of a portion of data.
 *
 * @package App
 */
class Dimensions
{
    /**
     * Contains the timestamp at which the post was created.
     *
     * @var Carbon
     */
    protected $_createdAt;

    /**
     * Contains geo location if available.
     *
     * @var string|null
     */
    protected $_geo;

    /**
     * Contains coordinates if available.
     *
     * @var string|null
     */
    protected $_coordinates;

    /**
     * Contains language identifier in which content was written.
     *
     * @var string
     */
    protected $_lang;

    /**
     * Default constructor.
     *
     * @param Carbon      $createdAt
     * @param string      $lang
     * @param string|null $geo
     * @param string|null $coordinates
     */
    public function __construct(Carbon $createdAt, string $lang, string $geo = null, string $coordinates = null)
    {
        $this->_createdAt = $createdAt;
        $this->_lang = $lang;
        $this->_geo = $geo;
        $this->_coordinates = $coordinates;
    }

    /**
     * Provides language identifier in which content was written.
     *
     * @return string
     */
    public function getLang() : string
    {
        return $this->_lang;
    }

    /**
     * Provides coordinates if available.
     *
     * @return string|null
     */
    public function getCoordinates() : ?string
    {
        return $this->_coordinates;
    }

    /**
     * Provides geo location if available.
     *
     * @return string|null
     */
    public function getGeo() : ?string
    {
        return $this->_geo;
    }

    /**
     * Provides the timestamp at which the post was created.
     *
     * @return Carbon
     */
    public function getCreatedAt() : Carbon
    {
        return $this->_createdAt;
    }
}
