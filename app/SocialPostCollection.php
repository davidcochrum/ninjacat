<?php
/**
 * @author David Cochrum <davidcochrum@gmail.com>
 */

namespace App;

use Illuminate\Support\Collection;

/**
 * Represents a collection of social media posts by a specific user.
 *
 * @package App
 */
class SocialPostCollection extends Collection
{
    /**
     * Contains display name of post author.
     *
     * @var string
     */
    protected $_authorName;

    /**
     * Contains identifier of post author.
     *
     * @var string
     */
    protected $_authorHandle;

    /**
     * Contains URL to access profile of post author.
     *
     * @var string
     */
    protected $_authorUrl;

    /**
     * Default constructor.
     *
     * @param string       $authorName   Display name of post author.
     * @param string       $authorHandle Identifier of post author.
     * @param string       $authorUrl    URL to access profile of post author.
     * @param SocialPost[] $posts        Optional list of post objects to initialize with.
     */
    public function __construct(string $authorName, string $authorHandle, string $authorUrl, array $posts = [])
    {
        parent::__construct($posts);

        $this->_authorName = $authorName;
        $this->_authorHandle = $authorHandle;
        $this->_authorUrl = $authorUrl;
    }

    /**
     * Provides display name of post author.
     *
     * @return string
     */
    public function getAuthorName() : string
    {
        return $this->_authorName;
    }

    /**
     * Provides identifier of post author.
     *
     * @return string
     */
    public function getAuthorHandle() : string
    {
        return $this->_authorHandle;
    }

    /**
     * Provides URL to access profile of post author.
     *
     * @return string
     */
    public function getAuthorUrl() : string
    {
        return $this->_authorUrl;
    }
}
