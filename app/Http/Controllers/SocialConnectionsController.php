<?php

namespace App\Http\Controllers;

use App\Repositories\SocialRepository;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

/**
 * Controller responsible for loading social media connection data from a repository into the appropriate view.
 *
 * @package App\Http\Controllers
 */
class SocialConnectionsController extends Controller
{
    /**
     * Contains repository from which data will be extracted.
     *
     * @var SocialRepository
     */
    protected $_repository;

    /**
     * Default constructor.
     *
     * @param SocialRepository $repository Repository from which data will be extracted.
     */
    public function __construct(SocialRepository $repository)
    {
        $this->_repository = $repository;
    }

    /**
     * Loads social media connection data into appropriate view.
     *
     * @param Request $request Current request.
     * @param string  $handle  User's social media handle.
     *
     * @return View
     */
    public function __invoke(Request $request, string $handle) : View
    {
        return view('social.connections',
            ['connections' => $this->_repository->getConnectionsForHandle($handle, 20, $request->get('cursor', -1))]);
    }
}
