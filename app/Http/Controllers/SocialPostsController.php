<?php

namespace App\Http\Controllers;

use App\Repositories\SocialRepository;
use Illuminate\Contracts\View\View;
use Illuminate\Routing\Controller;

/**
 * Controller responsible for loading social media post data from a repository into the appropriate view.
 *
 * @package App\Http\Controllers
 */
class SocialPostsController extends Controller
{
    /**
     * Contains repository from which data will be extracted.
     *
     * @var SocialRepository
     */
    protected $_repository;

    /**
     * Default constructor.
     *
     * @param SocialRepository $repository Repository from which data will be extracted.
     */
    public function __construct(SocialRepository $repository)
    {
        $this->_repository = $repository;
    }

    /**
     * Loads social media post data into appropriate view.
     *
     * @param string $handle User's social media handle.
     *
     * @return View
     */
    public function __invoke(string $handle) : View
    {
        return view('social.posts', ['posts' => $this->_repository->getPostsFromHandle($handle)]);
    }
}
