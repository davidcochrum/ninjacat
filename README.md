
# David Cochrum's NinjaCat Coding Exercise

As the ***top*** candidate in the running for the Integration Developer opening at NinjaCat, I have to submit my entry
for the coding exercise (more as a formality, of course). The purpose of this application is to read data from the
Twitter API regarding likes and retweets for each tweet made by a celebrity.

My celebrity of choice is Elon Musk because of his vision coming to life that is completely shaking up the slow moving
car industry. I couldn't help myself to just stop there, though. I took the liberty of creating the flexibility to
retrieve the same data for ***any*** Twitter handle. For convenience, I've included links in the header for a handful
of other ***high profile*** celebrities as well. For anyone else I forgot to include, simply enter your handle of choice
into the URL.

The underlying services at work in providing the data to be rendered were written to be as dynamic as possible. With a
little more work in the templates to swap out some of the Twitter-specific language, the controller logic would be
completely reusable for any number of social media services. To render data from another social media service, an
implementation of the `SocialRepository` interface would need to convert the API data into the various data models and
a group of routes would simply need to define that implementation as its repository.

My choice for the *Additional Item* was to include a table view of the user's followers. You'll find a link to the
followers just above the table of tweets. It's interesting to note that pagination is a bit tricky with this table since
certain people have more followers, to the tune of 23 million + for Elon. Twitter's API doesn't provide you with page
numbers to be able to jump around at will. You have to go one page at a time 😓.

### Try It Out
In order to run the application:

1. Execute the following commands:
   ```bash
   cd project/path
   composer install
   ```
2. Fill your Twitter API credentials in the `.env` file
   ```
   TWITTER_CONSUMER_KEY=consumerkey
   TWITTER_CONSUMER_SECRET=consumersecret
   TWITTER_ACCESS_TOKEN=accesstoken
   TWITTER_ACCESS_TOKEN_SECRET=accesstokensecret
   ```
3. Serve the application by running:
   ```bash
   php artisan serve
   ```

### TODO
- [ ] Unit tests
- [ ] Integration tests
- [ ] Implement other social media services
- [ ] Handle API errors with more user-friendly messages
- [ ] Switch the followers table to an infinite scroll so it's somewhat less tedious to scroll through
- [ ] Encourage Randar to tweet more and get more followers because he's lagging wayyy behind the others
