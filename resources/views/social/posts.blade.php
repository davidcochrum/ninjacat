@extends('layouts.app')

@section('title', 'Tweets from ' . $posts->getAuthorName())

@section('content')
    <h1 class="page-header">Tweets from {{ $posts->getAuthorName() }} (<a href="{{ $posts->getAuthorUrl() }}" target="_blank">{{ '@' . $posts->getAuthorHandle() }}</a>)</h1>
    <p><a href="{{ route('twitter.connections', ['handle' => request('handle')]) }}">Followers of {{ '@' . $posts->getAuthorHandle() }}</a></p>

    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
        <tr>
            <th>Date</th>
            <th>Tweet</th>
            <th>Likes</th>
            <th>Retweets</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($posts as $post)
            <tr class="
                    @if ($loop->index % 2 == 0)
                        even
                    @else
                        odd
                    @endif

                    @switch($loop->index % 3)
                    @case(0)
                            gradeX
                            @break

                    @case(1)
                            gradeC
                            @break

                    @case(2)
                            gradeA
                            @break
                    @endswitch
                    ">
                <td data-order="{{ $post->getDimensions()->getCreatedAt()->format('U') }}">
                    {{ $post->getDimensions()->getCreatedAt()->format('F jS, Y') }}
                </td>
                <td>{{ $post->getContent()  }}</td>
                <td class="text-right" data-order="{{ $post->getLikes() }}">
                    {{ number_format($post->getLikes()) }}
                </td>
                <td class="text-right" data-order="{{ $post->getShares() }}">
                    {{ number_format($post->getShares()) }}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table><!-- /.table-responsive -->
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                order: [[0, 'desc']],
                responsive: true
            });
        });
    </script>
@endsection
