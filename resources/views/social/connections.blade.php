@extends('layouts.app')

@section('title', 'Followers of ' . $connections->getUserName())

@section('content')
    <h1 class="page-header">Followers of {{ $connections->getUserName() }} (<a href="{{ $connections->getUserUrl() }}" target="_blank">{{ '@' . $connections->getUserHandle() }}</a>)</h1>
    <p><a href="{{ route('twitter.posts', ['handle' => request('handle')]) }}">Tweets from {{ '@' . $connections->getUserHandle() }}</a></p>
    <p><strong>{{ number_format($connections->getTotal()) }}</strong> total</p>

    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
        <tr>
            <th>Name</th>
            <th>Handle</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($connections as $connection)
            <tr class="
                    @if ($loop->index % 2 == 0)
                        even
                    @else
                        odd
                    @endif

                    @switch($loop->index % 3)
                    @case(0)
                        gradeX
                        @break

                    @case(1)
                        gradeC
                        @break

                    @case(2)
                        gradeA
                        @break
                    @endswitch
                    ">
                <td>{{ $connection->getName() }}</td>
                <td><a href="https://twitter.com/{{ $connection->getHandle() }}" target="_blank">{{ '@' . $connection->getHandle()  }}</a></td>
            </tr>
        @endforeach
        </tbody>
    </table><!-- /.table-responsive -->

    <nav aria-label="Followers navigation">
        <ul class="pagination justify-content-between">
            <li class="page-item"><a class="page-link" href="{{ route('twitter.connections', ['handle' => request('handle'), 'cursor' => $connections->getPrev()]) }}">Previous</a>
            <li class="page-item"><a class="page-link" href="{{ route('twitter.connections', ['handle' => request('handle'), 'cursor' => $connections->getNext()]) }}">Next</a>
        </ul>
    </nav>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                info: false,
                ordering: false,
                paging: false,
                responsive: true,
                searching: false,
            });
        });
    </script>
@endsection
